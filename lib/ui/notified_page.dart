import 'package:flutter/material.dart';
import 'package:flutter_local_push_notification/ui/theme.dart';
import 'package:flutter_local_push_notification/ui/widgets/input_field.dart';
import 'package:get/get.dart';

class NotifiedPage extends StatelessWidget {
  const NotifiedPage({Key? key, required this.label}) : super(key: key);

  final String label;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Get.isDarkMode ? Colors.grey[600] : Colors.white,
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: Icon(
            Icons.arrow_back_ios,
            color: Get.isDarkMode ? Colors.white : Colors.grey,
          ),
        ),
        title: const Text(
          'Chi tiết thông báo',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MyInputField(
              title: 'title',
              hint: '',
              widget: Container(),
              initialValue: this.label.toString().split("|")[0],
            ),
            MyInputField(
              title: 'Note',
              hint: '',
              widget: Container(),
              initialValue: this.label.toString().split("|")[1],
            ),
            MyInputField(
              title: 'Date',
              hint: '',
              widget: Container(),
              initialValue: this.label.toString().split("|")[2],
            ),
            Row(
              children: [
                Expanded(
                  child: MyInputField(
                    title: 'Start Date',
                    hint: '',
                    widget: Container(),
                    initialValue: this.label.toString().split("|")[3],
                  ),
                ),
                const SizedBox(width: 12),
                Expanded(
                  child: MyInputField(
                    title: 'End Date',
                    hint: '',
                    widget: Container(),
                    initialValue: this.label.toString().split("|")[4],
                  ),
                )
              ],
            ),
            MyInputField(
              title: 'Remind',
              hint: '',
              widget: Container(),
              initialValue: this.label.toString().split("|")[6],
            ),
            MyInputField(
              title: 'Repeat',
              hint: '',
              widget: Container(),
              initialValue: this.label.toString().split("|")[7],
            ),
            Column(
              children: [
                SizedBox(height: 20),
                Text(
                  'Color',
                  style: titleStyle,
                ),
                const SizedBox(height: 9.0),
                CircleAvatar(
                  radius: 14,
                  backgroundColor: this.label.toString().split("|")[5] == '0'
                      ? primaryClr
                      : this.label.toString().split("|")[5] == '1'
                          ? pinkClr
                          : yellowClr,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
